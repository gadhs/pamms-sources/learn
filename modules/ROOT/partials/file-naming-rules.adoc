== File Names

A file's name is used to create its page URL.
Changing a file’s name will break links, cross-references (xrefs) in other files, and incoming links to the page from external websites.

When a file name is changed all cross-references (xrefs) links in all other policy files would need to be updated to the new name.
This is why it is important that the file name be correct when initially created.

FILE NAME REQUIREMENTS:
====
* **Use** all lowercase letters.
* **Do not** use blank spaces.
Separate words with hyphens.
* **Save** AsciiDoc files with the _.adoc_ extension.
* **Do not** use symbols or special characters.
* **Do not** include references to a version of the file in the File Name.
** For example, 3000-v1.adoc or 3000-9-20-2024.adoc
====