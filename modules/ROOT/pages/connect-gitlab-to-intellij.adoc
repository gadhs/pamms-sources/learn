= Connecting GitLab account to IntelliJ IDEA

. Select the Customize tab and click the All Settings button to open the full settings dialog box.
+
image::annotated/intellij-all-settings.jpg[JetBrains]

. Select Settings > Version Control > GitLab and click Add Account.
+
image::annotated/intellij-settings-vcs.jpg[JetBrains]
+
image::annotated/intellij-settings-gitlab.jpg[JetBrains]

. The following dialog box will open prompting for a Token. Click the generate button.
+
image::annotated/intellij-gitlab-generate.jpg[JetBrains]

. A browser window will open to gitlab.com. Enter your GitLab credentials and log in.

. Once logged into GitLab you will be taken to the following Generate Token page. Click Add new Token.
+
image::annotated/gitlab-add-token.jpg[JetBrains]

. In the following page you will have the option to fine tune the permissions and duration of the token you are about to generate. Update the expiration date to a reasonable date in the future (~3 months) and confirm the 2 boxes in the screenshot are selected. Finally, click Create personal access Token.
+
image::annotated/gitlab-add-token-2.jpg[JetBrains]

. In the following page you can view and copy the generated token by clicking the indicated button,
+
image::annotated/gitlab-add-token-3.jpg[JetBrains]

. Go back to IntelliJ and enter the copied token and click Log In.
+
image::intellij-gitlab-enter-token.png[JetBrains]

. If successful, your GitLab account will show up as a connected account.
+
image::annotated/intellij-gitlab-connected.jpg[JetBrains]

. Click Apply and you should be taken back to the home screen from step 1.