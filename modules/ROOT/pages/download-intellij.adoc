= Downloading IntelliJ IDEA Community Edition

IDE’s (Integrated Development Environment) are a type of software that allows you to easily interact with code. Think of IDE’s as Word Processors (MS Word) for code. IntelliJ IDEA developed by JetBrains is the IDE that we’ll be using, but there are many others available. We’ll be using IntelliJ to edit policy files and run the git operations to commit and push your changes.

. Navigate to the JetBrains website and https://www.jetbrains.com/idea/download/download-thanks.html?platform=windows&code=IIC[download] the latest version of IntelliJ IDEA.
+
image::jetbrains-website.png[JetBrains]

. Click Download.
+
image::annotated/jetbrains-website-download-button.jpg[JetBrains]

. You will be taken to a page with download links for IntelliJ IDEA. Confirm that Windows is selected.
+
image::annotated/intellij-idea-website-windows-tab.jpg[JetBrains]

. Scroll down to Community Edition and click Download.
+
image::annotated/intellij-idea-website-2-community-edition-download.jpg[JetBrains]

. You will be taken to a page indicating the download has started.
+
image::annotated/intellij-idea-download-started.jpg[JetBrains]

[IMPORTANT]
If at any point you are prompted for Admin credentials - hit Cancel and continue.