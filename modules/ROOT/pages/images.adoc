= Images

The block image macro (`image::[]`) and the inline image macro (`image:[]`) accept SVG, PNG, JPG/JPEG, and GIF file formats.

[#insert-block-image]
== Insert a block image

Block images, indicated by two colons (`::`), are entered on their own line and displayed on their own line.
Optional attributes and values are entered inside the macro's attribute list (`[]`).
If the macro's attribute list is empty (e.g., `image::filename.jpg[]`), the image is displayed at its original size and centered on the page.

.Block image
[source#block,asciidoc]
----
include::example$images.adoc[tag=block]
----

The example above is published as:

include::example$images.adoc[tag=block]

To adjust the size and position of an image, you use the macro's attributes.
In the <<block-with-attributes,block image with attributes example>>, the first positional value is empty, so the opening square bracket (`[`) is directly followed by a comma (`,`).
The second positional value, `280`, indicates the maximum width of the image.
The next attribute, `align`, and its assigned value `center`, places the image in the center of the visitors screen.

If the macro's attribute list is empty (e.g., `image::filename.jpg[]`), the image is displayed at its original size and centered on the page.

.Block image with attributes
[source#block-with-attributes,asciidoc]
----
include::example$images.adoc[tag=block-attr]
----

The example above is published as:

include::example$images.adoc[tag=block-attr]

[#insert-inline-image]
== Insert an inline image

The inline image macro, indicated by a single colon (`:`), is used for displaying small images and icons directly in the flow of a sentence.

.Inline image
[source,asciidoc]
----
include::example$images.adoc[tag=inline]
----

The example above is published as:

include::example$images.adoc[tag=inline]