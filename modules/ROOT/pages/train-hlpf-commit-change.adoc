= Commit and Push updates

image::training/pamms-process-flow.jpg[JetBrains]

After each updated to a Policy document the update should be commited in IntelliJ.
A description of what is being updated should be included in the commit comments for tracking and clarity.
Only updates that have been commited are eligible to be pushed as an update.


== Prerequisites
* Completed xref:train-hlpf-create-issue.adoc[]
* Completed xref:train-hlpf-create-branch-from-issue.adoc[]
* Completed xref:train-hlpf-checkout-branch.adoc[]
* Completed xref:train-hlpf-create-merge-request.adoc[]

// Add section here to find the issue later

== Video Walkthrough
video::O-qRWsbs6GM[youtube, width=640, height=350]


== Steps
. From IntelliJ Idea navigate to the Program Project.
. Left-Click on the Branch Section in IntelliJ Idea to pull up the menu.
. On the drop-down menu select 'Commit'
+
image:training/train-hlpf-steps7a.jpg[JetBrains]
. This will open the commit view in IntelliJ.
You can also navigate to this view at any time using the IntelliJ’s Commit view by clicking the Commit view icon.
+
image:training/train-hlpf-steps7b.jpg[JetBrains]
. Update the Commit Message section with a logical summary of the changes being made for this commit.
Ensure all the files to be updated are selected and then click on the Commit button.
+
image:training/train-hlpf-steps7c.jpg[JetBrains]
. Once the commit is completed, you will be taken back to the file edit view.
To confirm that the changes you made were indeed committed you can view the commits on the branch.
+
image::annotated/intellij-git-icon.jpg[JetBrains]

. Clicking the git icon highlighted above will open a view that shows all the commits on the branch.
+
image::annotated/intellij-git-log.jpg[JetBrains]
